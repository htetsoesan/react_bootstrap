import React from 'react';
import './style.css';
import './wave.jpg';

function Loader() {
  return (
    <div className="parent">
        <div className="loader">
            <div className="loader-items">
                <div className='item' style={{'--i':'0'}}></div>
                <div className='item' style={{'--i':'1'}}></div>
                <div className='item' style={{'--i':'2'}}></div>
                <div className='item' style={{'--i':'3'}}></div>
                <div className='item' style={{'--i':'4'}}></div>
                <div className='item' style={{'--i':'5'}}></div>
                <div className='item' style={{'--i':'6'}}></div>
                <div className='item' style={{'--i':'7'}}></div> 
            </div>
            <div className="loader-title">
                <span>Loading...</span>
            </div>
        </div>
    </div>
  );
}

export default Loader;
